-- This query retrieves the total sales amount for each product category within a specified time period.

WITH TimeFilteredSales AS (
    SELECT
        s.prod_id,
        p.prod_category,
        s.amount_sold
    FROM
        sales s
    JOIN
        products p ON s.prod_id = p.prod_id
    JOIN
        times t ON s.time_id = t.time_id
    WHERE
        t.week_ending_day BETWEEN '1998-01-04' AND '1998-01-25'
)
SELECT
    prod_category,
    SUM(amount_sold) AS total_sales_amount
FROM
    TimeFilteredSales
GROUP BY
    prod_category;
   
-- This query calculates the average sales quantity by region for a specific product.   

WITH ProductRegionSales AS (
    SELECT
        c.country_region,
        s.cust_id,
        COUNT(*) AS total_sales
    FROM
        sales s
    JOIN
        customers cust ON s.cust_id = cust.cust_id
    JOIN
        countries c ON c.country_id = c.country_id
    WHERE
        s.prod_id = '21'
    GROUP BY
        c.country_region, s.cust_id
)
SELECT
    country_region,
    AVG(total_sales) AS avg_total_sales
FROM
    ProductRegionSales
GROUP BY
    country_region;

-- This query identifies the top five customers with the highest total sales amount.
   
WITH CustomerSales AS (
    SELECT
        cust.cust_id,
        cust.cust_first_name,
        cust.cust_last_name,
        SUM(s.amount_sold) AS total_sales_amount
    FROM
        sales s
    JOIN
        customers cust ON s.cust_id = cust.cust_id
    GROUP BY
        cust.cust_id, cust.cust_first_name, cust.cust_last_name
)
SELECT
    cust_id,
    cust_first_name,
    cust_last_name,
    total_sales_amount
FROM
    CustomerSales
ORDER BY
    total_sales_amount DESC
LIMIT 5;
